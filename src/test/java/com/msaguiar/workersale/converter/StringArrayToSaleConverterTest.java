package com.msaguiar.workersale.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Sale;
import com.msaguiar.workersale.model.SaleItem;

@ExtendWith(MockitoExtension.class)
class StringArrayToSaleConverterTest {

	@Mock
	private Converter<String, SaleItem> stringToSaleItemConverter;

	@InjectMocks
	private StringArrayToSaleConverter stringArrayToSaleConverter;

	@Test
	void convertNull() {

		String[] saleLine = null;
		Sale sale = stringArrayToSaleConverter.convert(saleLine);

		assertThat(sale).isNull();
	}

	@Test
	void convertInvalid() {

		String code = "003";
		String id = "10";
		String saleItens = "[1-10-100,2-30-2.50,3-40-3.10]";
		String[] saleLine = new String[] { code, id, saleItens };

		AssertionError ex = Assertions.assertThrows(AssertionError.class, () -> {
			stringArrayToSaleConverter.convert(saleLine);
		});

		assertThat(ex.getMessage()).isEqualTo(Messages.CONVERTER_SALE_CHECK);

	}

	@Test
	void convertSuccess() {

		String code = "003";
		String id = "10";
		String saleItens = "[1-10-100,2-30-2.50,3-40-3.10]";
		String salesman = "Pedro";
		String[] saleLine = new String[] { code, id, saleItens, salesman };

		@SuppressWarnings("unchecked")
		Collection<String> collection = any(Collection.class);
		Collection<SaleItem> saleItensMock = Collections.emptyList();
		when(stringToSaleItemConverter.convert(collection)).thenReturn(saleItensMock);

		Sale sale = stringArrayToSaleConverter.convert(saleLine);

		assertThat(sale).isNotNull();
		assertThat(sale.getId()).isEqualTo(Integer.valueOf(id));
		assertThat(sale.getSalesmanName()).isEqualTo(salesman);
		assertThat(sale.getItens()).isEqualTo(saleItensMock);

	}
}
