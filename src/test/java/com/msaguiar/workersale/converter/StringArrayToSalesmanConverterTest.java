package com.msaguiar.workersale.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Salesman;

@ExtendWith(MockitoExtension.class)
class StringArrayToSalesmanConverterTest {

	@InjectMocks
	private StringArrayToSalesmanConverter stringArrayToSalesmanConverter;

	@Test
	void convertNull() {

		String[] salesmanLine = null;
		Salesman salesman = stringArrayToSalesmanConverter.convert(salesmanLine);

		assertThat(salesman).isNull();
	}

	@Test
	void convertInvalid() {

		String code = "001";
		String cpf = "1234567891234";
		String salary = "50000";
		String[] salesmanLine = new String[] { code, cpf, salary };
		AssertionError ex = Assertions.assertThrows(AssertionError.class, () -> {
			stringArrayToSalesmanConverter.convert(salesmanLine);
		});

		assertThat(ex.getMessage()).isEqualTo(Messages.CONVERTER_SALESMAN_CHECK);

	}

	@Test
	void convertSuccess() {

		String code = "001";
		String cpf = "1234567891234";
		String name = "Pedro";
		String salary = "50000";
		String[] salesmanLine = new String[] { code, cpf, name, salary };
		Salesman salesman = stringArrayToSalesmanConverter.convert(salesmanLine);

		assertThat(salesman).isNotNull();
		assertThat(salesman.getCpf()).isEqualTo(cpf);
		assertThat(salesman.getName()).isEqualTo(name);
		assertThat(salesman.getSalary()).isEqualTo(BigDecimal.valueOf(Double.valueOf(salary)));

	}

}
