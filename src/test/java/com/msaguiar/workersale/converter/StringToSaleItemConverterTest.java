package com.msaguiar.workersale.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.SaleItem;

@ExtendWith(MockitoExtension.class)
class StringToSaleItemConverterTest {

	@InjectMocks
	private StringToSaleItemConverter stringToSaleItemConverter;

	@Test
	void convertNull() {

		String saleItemLine = null;
		SaleItem saleItem = stringToSaleItemConverter.convert(saleItemLine);

		assertThat(saleItem).isNull();
	}

	@Test
	void convertEmpty() {

		String saleItemLine = "";
		SaleItem saleItem = stringToSaleItemConverter.convert(saleItemLine);

		assertThat(saleItem).isNull();
	}

	@Test
	void convertInvalid() {

		int id = 151;
		int quantity = 5;

		String saleItemLine = String.join("-", Integer.toString(id), Integer.toString(quantity));

		AssertionError ex = Assertions.assertThrows(AssertionError.class, () -> {
			stringToSaleItemConverter.convert(saleItemLine);
		});

		assertThat(ex.getMessage()).isEqualTo(Messages.CONVERTER_SALE_ITEM_CHECK);

	}

	@Test
	void convertSuccess() {

		int id = 151;
		int quantity = 5;
		Double price = Double.valueOf(3.4);

		String saleItemLine = String.join("-", Integer.toString(id), Integer.toString(quantity), price.toString());
		SaleItem saleItem = stringToSaleItemConverter.convert(saleItemLine);

		assertThat(saleItem).isNotNull();
		assertThat(saleItem.getId()).isEqualTo(id);
		assertThat(saleItem.getQuantity()).isEqualTo(quantity);
		assertThat(saleItem.getPrice()).isEqualTo(BigDecimal.valueOf(price));

	}

}
