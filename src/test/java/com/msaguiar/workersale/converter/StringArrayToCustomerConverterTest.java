package com.msaguiar.workersale.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Customer;

@ExtendWith(MockitoExtension.class)
class StringArrayToCustomerConverterTest {

	@InjectMocks
	private StringArrayToCustomerConverter stringArrayToCustomerConverter;

	@Test
	void convertNull() {

		String[] customerLine = null;
		Customer customer = stringArrayToCustomerConverter.convert(customerLine);

		assertThat(customer).isNull();
	}

	@Test
	void convertInvalid() {

		String code = "002";
		String cnpj = "2345675434544345";
		String name = "Jose da Silva";
		String[] customerLine = new String[] { code, cnpj, name };

		AssertionError ex = Assertions.assertThrows(AssertionError.class, () -> {
			stringArrayToCustomerConverter.convert(customerLine);
		});

		assertThat(ex.getMessage()).isEqualTo(Messages.CONVERTER_CUSTOMER_CHECK);

	}

	@Test
	void convertSuccess() {

		String code = "002";
		String cnpj = "2345675434544345";
		String name = "Jose da Silva";
		String businessArea = "Rural";
		String[] customerLine = new String[] { code, cnpj, name, businessArea };
		Customer customer = stringArrayToCustomerConverter.convert(customerLine);

		assertThat(customer).isNotNull();
		assertThat(customer.getCnpj()).isEqualTo(cnpj);
		assertThat(customer.getName()).isEqualTo(name);
		assertThat(customer.getBusinessArea()).isEqualTo(businessArea);

	}

}
