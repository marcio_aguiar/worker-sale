package com.msaguiar.workersale.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.msaguiar.workersale.builder.Builders;
import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Customer;
import com.msaguiar.workersale.model.DataResult;
import com.msaguiar.workersale.model.Model;
import com.msaguiar.workersale.model.Sale;
import com.msaguiar.workersale.model.SaleItem;
import com.msaguiar.workersale.model.Salesman;

@ExtendWith(MockitoExtension.class)
public class SalesResultServiceImpTest {

	@InjectMocks
	private SalesResultServiceImpl salesResultServiceImpl;

	@Test
	void processResultWithListModalIsEmpty() {

		List<Model> listModel = Collections.emptyList();
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(0);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(0);
		assertThat(dataResult.getMostExpensiveSaleId()).isNull();
		assertThat(dataResult.getWorstSalesmanName()).isNull();

	}

	@Test
	void processResultWithListModalIsNull() {

		List<Model> listModel = null;
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(0);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(0);
		assertThat(dataResult.getMostExpensiveSaleId()).isNull();
		assertThat(dataResult.getWorstSalesmanName()).isNull();
	}

	@Test
	void processResultAmountOfCustomers() {

		Customer customer = Builders.customer().build();
		List<Model> listModel = Arrays.asList(customer);
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(1);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(0);
		assertThat(dataResult.getMostExpensiveSaleId()).isNull();
		assertThat(dataResult.getWorstSalesmanName()).isNull();

	}

	@Test
	void processResultAmountOfSalesmans() {

		Salesman salesman = Builders.salesman().build();
		List<Model> listModel = Arrays.asList(salesman);
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(0);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(1);
		assertThat(dataResult.getMostExpensiveSaleId()).isNull();
		assertThat(dataResult.getWorstSalesmanName()).isNull();

	}

	@Test
	void processResultAmountOfSaleWithoutItens() {

		Sale sale = Builders.sale().build();
		List<Model> listModel = Arrays.asList(sale);
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(0);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(0);
		assertThat(dataResult.getMostExpensiveSaleId()).isNull();
		assertThat(dataResult.getWorstSalesmanName()).isNull();

	}

	@Test
	void processResultAmountOfSaleEmpty() {

		SaleItem saleItem = Builders.saleItem().build();
		Collection<SaleItem> itens = Arrays.asList(saleItem);
		Sale sale = Builders//
				.sale()//
				.itens(itens)//
				.build();
		List<Model> listModel = Arrays.asList(sale);
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(0);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(0);
		assertThat(dataResult.getMostExpensiveSaleId()).isNull();
		assertThat(dataResult.getWorstSalesmanName()).isNull();

	}

	@Test
	void processResultAmountOfSaleSuccess() {

		String salesmanName = "Teste";
		Integer id = 101;
		SaleItem saleItem = Builders//
				.saleItem()//
				.price(BigDecimal.TEN.add(BigDecimal.ONE))//
				.quantity(10)//
				.build();

		Sale sale = Builders//
				.sale()//
				.itens(Arrays.asList(saleItem))//
				.id(id)//
				.salesmanName("SalesMan")//
				.build();

		SaleItem saleItem2 = Builders//
				.saleItem()//
				.price(BigDecimal.TEN)//
				.quantity(10)//
				.build();
		Sale sale2 = Builders//
				.sale()//
				.itens(Arrays.asList(saleItem2))//
				.id(110000)//
				.salesmanName(salesmanName)//
				.build();

		List<Model> listModel = Arrays.asList(sale2, sale);
		DataResult dataResult = salesResultServiceImpl.processResult(listModel);

		assertThat(dataResult).isNotNull();
		assertThat(dataResult.getAmountOfCustomers()).isEqualTo(0);
		assertThat(dataResult.getAmountOfSalesmans()).isEqualTo(0);

		assertThat(dataResult.getMostExpensiveSaleId()).isEqualTo(id);
		assertThat(dataResult.getWorstSalesmanName()).isEqualTo(salesmanName);

	}

	@Test
	void printResultWithDataResultNull() {

		DataResult dataResult = null;

		AssertionError ex = Assertions.assertThrows(AssertionError.class, () -> {
			salesResultServiceImpl.printResult(dataResult);
		});

		assertThat(ex.getMessage()).isEqualTo(Messages.PRINT_RESULT_CHECK);

	}

	@Test
	void printResultWithDataResultWithOutWorstSalesmanNameAndAmostExpensiveSaleId() {

		int amountOfCustomers = 15;
		int amountOfSalesmans = 20;
		DataResult dataResult = Builders//
				.dataResult()//
				.amountOfCustomers(amountOfCustomers)//
				.amountOfSalesmans(amountOfSalesmans)//
				.build();

		String result = salesResultServiceImpl.printResult(dataResult);

		String[] lines = result.split(System.lineSeparator());
		assertThat(lines.length).isEqualTo(2);
		assertThat("Quantidade de clientes no arquivo de entrada: " + amountOfCustomers).isEqualTo(lines[0]);
		assertThat("Quantidade de vendedores no arquivo de entrada: " + amountOfSalesmans).isEqualTo(lines[1]);
	}

	@Test
	void printResultWithDataResultComplete() {

		int amountOfCustomers = 15;
		int amountOfSalesmans = 20;
		Integer mostExpensiveSaleId = Integer.valueOf(1567);
		String worstSalesmanName = "TESTE";
		DataResult dataResult = Builders//
				.dataResult()//
				.amountOfCustomers(amountOfCustomers)//
				.amountOfSalesmans(amountOfSalesmans)//
				.mostExpensiveSaleId(mostExpensiveSaleId)//
				.worstSalesmanName(worstSalesmanName)//
				.build();

		String result = salesResultServiceImpl.printResult(dataResult);

		String[] lines = result.split(System.lineSeparator());
		assertThat(lines.length).isEqualTo(4);
		assertThat("Quantidade de clientes no arquivo de entrada: " + amountOfCustomers).isEqualTo(lines[0]);
		assertThat("Quantidade de vendedores no arquivo de entrada: " + amountOfSalesmans).isEqualTo(lines[1]);
		assertThat("ID da venda mais cara: " + mostExpensiveSaleId).isEqualTo(lines[2]);
		assertThat("O pior vendedor: " + worstSalesmanName).isEqualTo(lines[3]);
	}

}
