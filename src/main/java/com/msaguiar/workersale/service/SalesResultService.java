package com.msaguiar.workersale.service;

import java.util.List;

import com.msaguiar.workersale.model.DataResult;
import com.msaguiar.workersale.model.Model;

public interface SalesResultService {

	DataResult processResult(List<Model> listModel);

	String printResult(DataResult dataResult);

}
