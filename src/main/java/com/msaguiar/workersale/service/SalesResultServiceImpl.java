package com.msaguiar.workersale.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;

import com.msaguiar.workersale.builder.Builders;
import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Customer;
import com.msaguiar.workersale.model.DataResult;
import com.msaguiar.workersale.model.Model;
import com.msaguiar.workersale.model.Sale;
import com.msaguiar.workersale.model.SaleItem;
import com.msaguiar.workersale.model.Salesman;

public class SalesResultServiceImpl implements SalesResultService {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	@Override
	public DataResult processResult(List<Model> listModel) {

		if (CollectionUtils.isEmpty(listModel)) {
			return Builders.dataResult().build();
		}

		int amountOfCustomers = 0;
		int amountOfSalesmans = 0;
		Integer mostExpensiveSaleId = null;
		String worstSalesmanName = null;

		BigDecimal salePriceMax = BigDecimal.ZERO;
		BigDecimal saleValueMin = BigDecimal.ZERO;

		for (Model model : listModel) {
			if (model instanceof Customer) {
				amountOfCustomers++;
			} else if (model instanceof Salesman) {
				amountOfSalesmans++;
			} else if (model instanceof Sale) {
				Sale sale = (Sale) model;
				BigDecimal salePrice = totalValueOfSale(sale);

				if (salePrice.compareTo(salePriceMax) == 1) {
					salePriceMax = salePrice;
					mostExpensiveSaleId = sale.getId();
				}

				if (salePrice.compareTo(saleValueMin) == -1 || BigDecimal.ZERO.equals(saleValueMin)) {
					saleValueMin = salePrice;
					worstSalesmanName = sale.getSalesmanName();
				}

			}
		}

		return Builders//
				.dataResult()//
				.amountOfCustomers(amountOfCustomers)//
				.amountOfSalesmans(amountOfSalesmans)//
				.mostExpensiveSaleId(mostExpensiveSaleId)//
				.worstSalesmanName(worstSalesmanName)//
				.build();
	}

	private BigDecimal totalValueOfSale(Sale sale) {
		Collection<SaleItem> itens = sale.getItens();
		if (CollectionUtils.isEmpty(itens)) {
			return BigDecimal.ZERO;
		}
		return itens.stream()//
				.map(item -> BigDecimal.valueOf(item.getQuantity())//
						.multiply(item.getPrice()))//
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	@Override
	public String printResult(DataResult dataResult) {

		assert dataResult != null : Messages.PRINT_RESULT_CHECK;

		StringBuilder builder = new StringBuilder();
		builder.append("Quantidade de clientes no arquivo de entrada: ");
		builder.append(dataResult.getAmountOfCustomers());
		builder.append(LINE_SEPARATOR);
		builder.append("Quantidade de vendedores no arquivo de entrada: ");
		builder.append(dataResult.getAmountOfSalesmans());
		builder.append(LINE_SEPARATOR);

		Integer mostExpensiveSaleId = dataResult.getMostExpensiveSaleId();
		if (Objects.nonNull(mostExpensiveSaleId)) {
			builder.append("ID da venda mais cara: ");
			builder.append(mostExpensiveSaleId);
			builder.append(LINE_SEPARATOR);
		}

		String worstSalesmanName = dataResult.getWorstSalesmanName();
		if (Objects.nonNull(mostExpensiveSaleId)) {
			builder.append("O pior vendedor: ");
			builder.append(worstSalesmanName);
			builder.append(LINE_SEPARATOR);
		}
		return builder.toString();
	}

}
