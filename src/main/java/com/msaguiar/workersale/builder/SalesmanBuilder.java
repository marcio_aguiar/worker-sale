package com.msaguiar.workersale.builder;

import java.math.BigDecimal;

import com.msaguiar.workersale.model.Salesman;

public class SalesmanBuilder implements Builder<Salesman> {

	private String cpf;
	private String name;
	private BigDecimal salary;

	SalesmanBuilder() {
	}

	public SalesmanBuilder cpf(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public SalesmanBuilder name(String name) {
		this.name = name;
		return this;
	}

	public SalesmanBuilder salary(BigDecimal salary) {
		this.salary = salary;
		return this;
	}

	@Override
	public Salesman build() {
		Salesman salesman = new Salesman();
		salesman.setCpf(cpf);
		salesman.setName(name);
		salesman.setSalary(salary);
		return salesman;
	}

}
