package com.msaguiar.workersale.builder;

import java.math.BigDecimal;

import com.msaguiar.workersale.model.SaleItem;

public class SaleItemBuilder implements Builder<SaleItem> {

	private long id;
	private long quantity;
	private BigDecimal price = BigDecimal.ZERO;

	SaleItemBuilder() {
	}

	public SaleItemBuilder id(long id) {
		this.id = id;
		return this;
	}

	public SaleItemBuilder quantity(long quantity) {
		this.quantity = quantity;
		return this;
	}

	public SaleItemBuilder price(BigDecimal price) {
		if (price != null) {
			this.price = price;
		}
		return this;
	}

	@Override
	public SaleItem build() {
		SaleItem saleItem = new SaleItem();
		saleItem.setId(id);
		saleItem.setQuantity(quantity);
		saleItem.setPrice(price);
		return saleItem;

	}

}
