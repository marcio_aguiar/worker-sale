package com.msaguiar.workersale.builder;

public interface Builder<T> {
	T build();

}
