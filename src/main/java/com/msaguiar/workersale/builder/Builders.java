package com.msaguiar.workersale.builder;

public class Builders {

	public static SalesmanBuilder salesman() {
		return new SalesmanBuilder();
	}

	public static SaleBuilder sale() {
		return new SaleBuilder();
	}

	public static CustomerBuilder customer() {
		return new CustomerBuilder();
	}

	public static SaleItemBuilder saleItem() {
		return new SaleItemBuilder();
	}

	public static DataResultBuilder dataResult() {
		return new DataResultBuilder();
	}

}
