package com.msaguiar.workersale.builder;

import com.msaguiar.workersale.model.Customer;

public class CustomerBuilder implements Builder<Customer> {

	private String cnpj;
	private String name;
	private String businessArea;

	CustomerBuilder() {
	}

	public CustomerBuilder cnpj(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public CustomerBuilder name(String name) {
		this.name = name;
		return this;
	}

	public CustomerBuilder businessArea(String businessArea) {
		this.businessArea = businessArea;
		return this;
	}

	@Override
	public Customer build() {
		Customer customer = new Customer();
		customer.setBusinessArea(businessArea);
		customer.setCnpj(cnpj);
		customer.setName(name);
		return customer;
	}

}
