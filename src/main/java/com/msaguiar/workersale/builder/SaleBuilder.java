package com.msaguiar.workersale.builder;

import java.util.Collection;

import com.msaguiar.workersale.model.Sale;
import com.msaguiar.workersale.model.SaleItem;

public class SaleBuilder implements Builder<Sale> {

	private int id;
	private Collection<SaleItem> itens;
	private String salesmanName;

	SaleBuilder() {
	}

	public SaleBuilder id(int id) {
		this.id = id;
		return this;
	}

	public SaleBuilder itens(Collection<SaleItem> itens) {
		this.itens = itens;
		return this;
	}

	public SaleBuilder salesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
		return this;
	}

	@Override
	public Sale build() {
		Sale sale = new Sale();
		sale.setId(id);
		sale.setItens(itens);
		sale.setSalesmanName(salesmanName);
		return sale;
	}

}
