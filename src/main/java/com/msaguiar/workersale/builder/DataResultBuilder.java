package com.msaguiar.workersale.builder;

import com.msaguiar.workersale.model.DataResult;

public class DataResultBuilder implements Builder<DataResult> {

	private int amountOfCustomers;
	private int amountOfSalesmans;
	private Integer mostExpensiveSaleId;
	private String worstSalesmanName;

	DataResultBuilder() {
	}

	public DataResultBuilder amountOfCustomers(int amountOfCustomers) {
		this.amountOfCustomers = amountOfCustomers;
		return this;
	}

	public DataResultBuilder amountOfSalesmans(int amountOfSalesmans) {
		this.amountOfSalesmans = amountOfSalesmans;
		return this;
	}

	public DataResultBuilder mostExpensiveSaleId(Integer mostExpensiveSaleId) {
		this.mostExpensiveSaleId = mostExpensiveSaleId;
		return this;
	}

	public DataResultBuilder worstSalesmanName(String worstSalesmanName) {
		this.worstSalesmanName = worstSalesmanName;
		return this;
	}

	@Override
	public DataResult build() {

		DataResult dataResult = new DataResult();
		dataResult.setAmountOfCustomers(amountOfCustomers);
		dataResult.setAmountOfSalesmans(amountOfSalesmans);
		dataResult.setMostExpensiveSaleId(mostExpensiveSaleId);
		dataResult.setWorstSalesmanName(worstSalesmanName);

		return dataResult;
	}

}
