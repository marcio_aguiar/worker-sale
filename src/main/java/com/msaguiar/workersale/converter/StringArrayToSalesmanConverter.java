package com.msaguiar.workersale.converter;

import java.math.BigDecimal;

import com.msaguiar.workersale.builder.Builders;
import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Salesman;

public class StringArrayToSalesmanConverter implements Converter<String[], Salesman> {

	private static final int CPF_POSITION = 1;
	private static final int NAME_POSITION = 2;
	private static final int SALARY = 3;
	private static final int MIN_LENGTH = 3;

	@Override
	public Salesman convert(String[] source) {

		if (source == null) {
			return null;
		}

		assert source.length > MIN_LENGTH : Messages.CONVERTER_SALESMAN_CHECK;

		Double salary = Double.parseDouble(source[SALARY]);

		return Builders//
				.salesman()//
				.cpf(source[CPF_POSITION])//
				.name(source[NAME_POSITION])//
				.salary(BigDecimal.valueOf(salary))//
				.build();

	}

}
