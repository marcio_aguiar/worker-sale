package com.msaguiar.workersale.converter;

import java.util.Arrays;

import com.msaguiar.workersale.builder.Builders;
import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Sale;
import com.msaguiar.workersale.model.SaleItem;

public class StringArrayToSaleConverter implements Converter<String[], Sale> {

	private static final String REGEX = "\\[(.*)\\]";
	private static final int SALE_ID_POSITION = 1;
	private static final int ITENS_POSITION = 2;
	private static final int SALLER_NAME_POSITION = 3;
	private static final int MIN_LENGTH = 3;

	private Converter<String, SaleItem> stringToSaleItemConverter;

	public StringArrayToSaleConverter(Converter<String, SaleItem> stringToSaleItemConverter) {
		this.stringToSaleItemConverter = stringToSaleItemConverter;
	}

	@Override
	public Sale convert(String[] source) {

		if (source == null) {
			return null;
		}

		assert source.length > MIN_LENGTH : Messages.CONVERTER_SALE_CHECK;

		String itens = source[ITENS_POSITION];

		String replaceAll = itens.replaceAll(REGEX, "$1");
		String[] itensToArray = replaceAll.split(",");

		return Builders//
				.sale()//
				.id(Integer.valueOf(source[SALE_ID_POSITION]))//
				.salesmanName(source[SALLER_NAME_POSITION])//
				.itens(stringToSaleItemConverter//
						.convert(Arrays.asList(itensToArray)))//
				.build();
	}

}
