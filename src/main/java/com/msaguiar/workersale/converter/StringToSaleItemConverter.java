package com.msaguiar.workersale.converter;

import java.math.BigDecimal;

import org.springframework.util.StringUtils;

import com.msaguiar.workersale.builder.Builders;
import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.SaleItem;

public class StringToSaleItemConverter implements Converter<String, SaleItem> {

	private static final String ITEM_SEPARATOR = "-";
	private static final int ITEM_ID_POSITION = 0;
	private static final int QUANTITY_POSITION = 1;
	private static final int PRICE_POSITION = 2;
	private static final int MIN_LENGTH = 2;

	@Override
	public SaleItem convert(String source) {

		if (StringUtils.isEmpty(source)) {
			return null;
		}

		String[] itens = source.split(ITEM_SEPARATOR);

		assert itens.length > MIN_LENGTH : Messages.CONVERTER_SALE_ITEM_CHECK;

		Double prince = Double.valueOf(itens[PRICE_POSITION]);

		return Builders//
				.saleItem()//
				.id(Long.valueOf(itens[ITEM_ID_POSITION]))//
				.quantity(Long.valueOf(itens[QUANTITY_POSITION]))//
				.price(BigDecimal.valueOf(prince))//
				.build();

	}

}
