package com.msaguiar.workersale.converter;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public interface Converter<S, T> {

	T convert(S s);

	default Collection<T> convert(Collection<S> collection) {
		if (collection == null) {
			return Collections.emptyList();
		}
		return collection.stream()//
				.map(this::convert)//
				.collect(Collectors.toList());
	}

}
