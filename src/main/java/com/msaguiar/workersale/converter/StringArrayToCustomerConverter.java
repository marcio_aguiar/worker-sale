package com.msaguiar.workersale.converter;

import com.msaguiar.workersale.builder.Builders;
import com.msaguiar.workersale.constants.Messages;
import com.msaguiar.workersale.model.Customer;

public class StringArrayToCustomerConverter implements Converter<String[], Customer> {

	private static final int CNPJ_POSITION = 1;
	private static final int NAME_POSITION = 2;
	private static final int BUSINESS_AREA_POSITION = 3;
	private static final int MIN_LENGTH = 3;

	@Override
	public Customer convert(String[] source) {
		if (source == null) {
			return null;
		}
		assert source.length > MIN_LENGTH : Messages.CONVERTER_CUSTOMER_CHECK;

		String cnpj = source[CNPJ_POSITION];
		String name = source[NAME_POSITION];
		String businessArea = source[BUSINESS_AREA_POSITION];
		return Builders//
				.customer()//
				.cnpj(cnpj)//
				.name(name)//
				.businessArea(businessArea)//
				.build();
	}

}
