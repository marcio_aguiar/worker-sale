package com.msaguiar.workersale.constants;

public class Messages {

	public static final String CONVERTER_CUSTOMER_CHECK = "Valor para cliente inválido.";
	public static final String CONVERTER_SALE_CHECK = "Valor para venda inválido.";
	public static final String CONVERTER_SALESMAN_CHECK = "Valor para vendedor inválido.";
	public static final String CONVERTER_SALE_ITEM_CHECK = "Valor para item de venda e inválido.";
	public static final String PRINT_RESULT_CHECK = "Resultado do processo inválido.";

}
