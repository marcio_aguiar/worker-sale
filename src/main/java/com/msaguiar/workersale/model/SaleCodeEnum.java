package com.msaguiar.workersale.model;

import org.springframework.util.StringUtils;

public enum SaleCodeEnum {
	SALLER("001", "stringArrayToSalesmanConverter"), //
	CUSTUMER("002", "stringArrayToCustomerConverter"), //
	SALE("003", "stringArrayToSaleConverter");

	private String code;
	private String converterName;

	private SaleCodeEnum(String code, String converterName) {
		this.code = code;
		this.converterName = converterName;
	}

	public String getCode() {
		return code;
	}

	public String getConverterName() {
		return converterName;
	}

	public static SaleCodeEnum valueWithCode(String code) {
		if (StringUtils.isEmpty(code)) {
			return null;
		}

		for (SaleCodeEnum saleCodeEnum : values()) {
			if (saleCodeEnum.code.equals(code)) {
				return saleCodeEnum;
			}
		}

		return null;
	}

}
