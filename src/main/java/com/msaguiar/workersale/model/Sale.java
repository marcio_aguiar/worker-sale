package com.msaguiar.workersale.model;

import java.util.Collection;

public class Sale implements Model {

	private int id;
	private Collection<SaleItem> itens;
	private String salesmanName;

	public int getId() {
		return id;
	}

	public Collection<SaleItem> getItens() {
		return itens;
	}

	public String getSalesmanName() {
		return salesmanName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setItens(Collection<SaleItem> itens) {
		this.itens = itens;
	}

	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}

}
