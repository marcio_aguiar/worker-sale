package com.msaguiar.workersale.model;

public class Customer implements Model {

	private String cnpj;
	private String name;
	private String businessArea;

	public String getBusinessArea() {
		return businessArea;
	}

	public String getCnpj() {
		return cnpj;
	}

	public String getName() {
		return name;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setName(String name) {
		this.name = name;
	}

}
