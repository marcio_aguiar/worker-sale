package com.msaguiar.workersale.model;

import java.math.BigDecimal;

public class SaleItem {

	private long id;
	private long quantity;
	private BigDecimal price;

	public long getId() {
		return id;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

}
