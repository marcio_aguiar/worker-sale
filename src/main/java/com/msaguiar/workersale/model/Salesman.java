package com.msaguiar.workersale.model;

import java.math.BigDecimal;

import com.msaguiar.workersale.model.Model;

public class Salesman implements Model {
	private String cpf;
	private String name;
	private BigDecimal salary;

	public String getCpf() {
		return cpf;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

}
