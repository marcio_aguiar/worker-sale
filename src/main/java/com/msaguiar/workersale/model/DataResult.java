package com.msaguiar.workersale.model;

public class DataResult {

	private int amountOfCustomers;
	private int amountOfSalesmans;
	private Integer mostExpensiveSaleId;
	private String worstSalesmanName;

	public int getAmountOfCustomers() {
		return amountOfCustomers;
	}

	public int getAmountOfSalesmans() {
		return amountOfSalesmans;
	}

	public Integer getMostExpensiveSaleId() {
		return mostExpensiveSaleId;
	}

	public String getWorstSalesmanName() {
		return worstSalesmanName;
	}

	public void setAmountOfCustomers(int amountOfCustomers) {
		this.amountOfCustomers = amountOfCustomers;
	}

	public void setAmountOfSalesmans(int amountOfSalesmans) {
		this.amountOfSalesmans = amountOfSalesmans;
	}

	public void setMostExpensiveSaleId(Integer mostExpensiveSaleId) {
		this.mostExpensiveSaleId = mostExpensiveSaleId;
	}

	public void setWorstSalesmanName(String worstSalesmanName) {
		this.worstSalesmanName = worstSalesmanName;
	}

}
