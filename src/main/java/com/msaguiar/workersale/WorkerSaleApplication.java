package com.msaguiar.workersale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkerSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkerSaleApplication.class, args);
	}

}

