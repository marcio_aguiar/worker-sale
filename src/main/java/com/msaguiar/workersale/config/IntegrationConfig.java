package com.msaguiar.workersale.config;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.integration.transformer.GenericTransformer;

import com.msaguiar.workersale.config.properties.WokerSalesProperties;
import com.msaguiar.workersale.converter.Converter;
import com.msaguiar.workersale.model.DataResult;
import com.msaguiar.workersale.model.Model;
import com.msaguiar.workersale.model.SaleCodeEnum;
import com.msaguiar.workersale.service.SalesResultService;

@Configuration
@EnableIntegration
public class IntegrationConfig {

	private final WokerSalesProperties workerSalesProperties;

	private final SalesResultService salesResultService;

	private Map<String, Converter<String[], ? extends Model>> converters;

	@Autowired
	public IntegrationConfig(WokerSalesProperties workerSalesProperties, SalesResultService salesResultService, Map<String, Converter<String[], ? extends Model>> converters) {
		this.workerSalesProperties = workerSalesProperties;
		this.salesResultService = salesResultService;
		this.converters = converters;
	}

	@Bean
	public IntegrationFlow flow() {
		return IntegrationFlows//
				.from(fileReadingMessageSource(), spec -> spec.poller(poller()))//
				.transform(Files.toStringTransformer())//
				.split(splitter -> splitter.delimiters(System.lineSeparator()))//
				.transform(transformLineInArray())//
				.transform(extracted())//
				.aggregate().handle(generateResultHandler())//
				.handle(resultToStirngHandler())//
				.handle(fileWritingMessageHandler())//
				.get();
	}

	private GenericTransformer<String[], ? extends Model> extracted() {
		return (String[] content) -> {

			String converterName = SaleCodeEnum.valueWithCode(content[0]).getConverterName();
			return converters.get(converterName).convert(content);
		};
	}

	private GenericTransformer<String, String[]> transformLineInArray() {
		return line -> Arrays//
				.stream(line.split(workerSalesProperties.getSeparator()))//
				.toArray(String[]::new);
	}

	@Bean
	public GenericHandler<List<Model>> generateResultHandler() {
		return (message, headers) -> {
			return salesResultService.processResult(message);
		};
	}

	@Bean
	public GenericHandler<DataResult> resultToStirngHandler() {
		return (message, headers) -> {
			return salesResultService.printResult(message);
		};
	}

	@Bean
	public PollerMetadata poller() {
		return Pollers//
				.fixedRate(workerSalesProperties.getPeriodPoller())//
				.get();
	}

	@Bean
	public FileReadingMessageSource fileReadingMessageSource() {
		return Files.inboundAdapter(Paths.get(workerSalesProperties.getDirIn()).toFile())//
				.useWatchService(true)//
				.get();
	}

	@Bean
	public FileWritingMessageHandler fileWritingMessageHandler() {

		return Files.outboundAdapter(Paths.get(workerSalesProperties.getDirOut()).toFile())//
				.deleteSourceFiles(true)//
				.fileExistsMode(FileExistsMode.REPLACE)//
				.get();
	}

}
