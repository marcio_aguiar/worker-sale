package com.msaguiar.workersale.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.msaguiar.workersale.converter.Converter;
import com.msaguiar.workersale.converter.StringArrayToCustomerConverter;
import com.msaguiar.workersale.converter.StringArrayToSaleConverter;
import com.msaguiar.workersale.converter.StringArrayToSalesmanConverter;
import com.msaguiar.workersale.converter.StringToSaleItemConverter;
import com.msaguiar.workersale.model.Customer;
import com.msaguiar.workersale.model.Sale;
import com.msaguiar.workersale.model.SaleItem;
import com.msaguiar.workersale.model.Salesman;
import com.msaguiar.workersale.service.SalesResultService;
import com.msaguiar.workersale.service.SalesResultServiceImpl;

@Configuration
public class WokerSalesConfig {

	@Bean
	public SalesResultService salesResultService() {
		return new SalesResultServiceImpl();
	}

	@Bean(name = "stringArrayToCustomerConverter")
	public Converter<String[], Customer> stringArrayToCustomerConverter() {
		return new StringArrayToCustomerConverter();
	}

	@Bean(name = "stringArrayToSalesmanConverter")
	public Converter<String[], Salesman> stringArrayToSalesmanConverter() {
		return new StringArrayToSalesmanConverter();
	}

	@Bean(name = "stringArrayToSaleConverter")
	public Converter<String[], Sale> stringArrayToSaleConverter() {
		return new StringArrayToSaleConverter(stringToSaleItemConverter());
	}

	@Bean(name = "stringToSaleItemConverter")
	public Converter<String, SaleItem> stringToSaleItemConverter() {
		return new StringToSaleItemConverter();
	}

}
