package com.msaguiar.workersale.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "woker-sales")
public class WokerSalesProperties {

	private int periodPoller;

	private String dirIn;

	private String dirOut;

	private String separator;

	public String getDirIn() {
		return dirIn;
	}

	public String getDirOut() {
		return dirOut;
	}

	public int getPeriodPoller() {
		return periodPoller;
	}

	public void setPeriodPoller(int periodPoller) {
		this.periodPoller = periodPoller;
	}

	public void setDirIn(String dirIn) {
		this.dirIn = dirIn;
	}

	public void setDirOut(String dirOut) {
		this.dirOut = dirOut;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

}
