
# Objetivo da prova:
O objetivo da prova é testarmos suas habilidades em desenvolvimento de software. Iremos
	avaliar mais do que o funcionamento da solução proposta, avaliaremos a sua abordagem, a
	sua capacidade analítica, boas práticas de engenharia de software, performance e
	escalabilidade da solução.

# Stack Utilizada
* Java 8 
* Spring Boot
* Spring Integration
	* Spring Integration DSL
	* Spring Integration File Support
* Jnuit 5
* Mockito

# Descrição da Solução
Optei em utilizar o Spring Integration para controlar o processo de leitura, transformação e escrita do resultado, deixando este controle sob responsabilidade do framework, não precisando ser implementado.
Criei o enum `SaleCodeEnum` para manter ter um relacionamento entre o código que defini a tipagem do objeto ao converter responsável em resolver este objeto. 

Criei a interface `SalesResultService` e sua implementação `SalesResultServiceImpl` para manter o desacoplamento.

Criei a classe `WokerSalesConfig` para criação dos beans, tantos service e convertes,
Utilizei esta abordagem pensando em deixar a implementação da solução desacoplada do framewok.


